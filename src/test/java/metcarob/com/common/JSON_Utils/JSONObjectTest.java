package metcarob.com.common.JSON_Utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class JSONObjectTest {

	@Test
	public void testGetFieldValue() throws Exception {
		
		JSONBase jb = JSON_Converter.getJSONObject("{\"access_token\":\"b32443mwvt1Yo0EuJ7SVAh3FkgSg9l87x\",\"token_type\":\"bearer\",\"expires_in\":3599}");
		JSONObject jobj = (JSONObject) jb;
		
		assert (jobj.getChildField("access_token")) != null;
		
		assertEquals("b32443mwvt1Yo0EuJ7SVAh3FkgSg9l87x",jobj.getChildField("access_token").getValue().getStringValue());
		assertEquals("bearer",jobj.getChildField("token_type").getValue().getStringValue());
		assertEquals("3599",jobj.getChildField("expires_in").getValue().getStringValue());
		
	}

	@Test
	public void testQuotes() throws Exception {
		JSONBase jb = JSON_Converter.getJSONObject("{\"aa\":\"\",\"bb\":\\\"}");
		JSONObject jobj = (JSONObject) jb;
		
		assert (jobj.getChildField("aa")) != null;
		assertEquals("",jobj.getChildField("aa").getValue().getStringValue());
		assertEquals("\"",jobj.getChildField("bb").getValue().getStringValue());
		
	}
}
