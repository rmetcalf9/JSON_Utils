package metcarob.com.common.JSON_Utils;

import static org.junit.Assert.*;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class JSON_ConverterTest {
	
	private void runExpcetedResultTest(String p_JSON, String p_expectedResult) throws Exception {

		String actualResult = JSON_Converter.ConvertJSONtoXML(p_JSON);
	    if (!actualResult.equals(p_expectedResult)) {
	    	System.out.println("actual:" + actualResult + ":\nexp:" + p_expectedResult + ":");
	    	fail("Didn't get expected result");
	    }
	    
	    
	    //Make sure actual result is valid XML
	    // this should not be needed as we have already checked that the expected result matches
	    // once tests are finalized we can comment this out for speed
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
        actualResult = "<rootEle>" + actualResult + "</rootEle>";
        
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(actualResult));
        Document doc = null;
        try { 
            doc = dBuilder.parse(is);            
        } catch (Exception ee) {
        	System.out.println("Failed to parse XML-" + actualResult);
            fail("XML result failed");
        }
		
	}

	@Test
	public void testFieldWithMissingValue() throws Exception {
	    String testJSONResponse="[{\"id\":\"77\",\"published\":}]";

	    String expectedResult = "";
		
	    boolean except_thrown = false;
	    try {
	    	String actualResult = JSON_Converter.ConvertJSONtoXML(testJSONResponse);
	    } catch (InvalidFieldValueException e) {
	    	except_thrown = true;
	    }
	    if (!except_thrown) fail("Failed to throw exception");
	    
	}
	
	@Test
	public void testArrayOfFieldsWithParentObject() throws Exception {
		//You can't have fields inside an array
		String testJSON="{[f1:v1,f2:v2,f3:v3]}";
	    boolean except_thrown = false;
	    try {
	    	String actualResult = JSON_Converter.ConvertJSONtoXML(testJSON);
	    } catch (InvalidFieldValueException e) {
	    	except_thrown = true;
	    }
	    if (!except_thrown) fail("Failed to throw exception");
	}

	@Test
	public void testArrayOfFieldsWithParentField() throws Exception {
		String testJSON="aa:{[f1:v1,f2:v2,f3:v3]}";

	    boolean except_thrown = false;
	    try {
	    	String actualResult = JSON_Converter.ConvertJSONtoXML(testJSON);
	    } catch (InvalidFieldValueException e) {
	    	except_thrown = true;
	    }
	    if (!except_thrown) fail("Failed to throw exception");
	}
	
	@Test
	public void testFieldWithArrayValueNoParent() throws Exception {
		String testJSON="aa:[v1,v2,v3]";
		String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aa>v1</aa><aa>v2</aa><aa>v3</aa></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}	

	@Test
	public void testFieldWithArrayValueObjectParent() throws Exception {
		String testJSON="{aa:[v1,v2,v3]}";
		String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aa>v1</aa><aa>v2</aa><aa>v3</aa></res>";

		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	@Test
	public void testArrayWithEmptyObject() throws Exception {
	    String testJSON="[{}]";
	    //Result should not be empty - this array has one child!
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}	
	
	@Test
	public void testObjectWithEmptyArray() throws Exception {
	    String testJSON="{[]}";
	    //JSON objects should always be present even if there is no data inside them
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}	
	
	@Test
	public void testFieldWithEmptyArray() throws Exception {
	    String testJSON="\"fn\":[]";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><fn xsi:nil=\"true\"/></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}	
	
	@Test
	public void testFieldWithEmptyArrayInsideObject() throws Exception {
	    String testJSON="{\"fn\":[]}";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><fn xsi:nil=\"true\"/></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}	
	
	@Test
	public void testEmptyObject() throws Exception {
	    String testJSON="{}";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}		
	
	@Test
	public void testEmptyArray() throws Exception {
	    String testJSON="[]";
	    String expectedResult = "";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}	
	
	@Test
	public void testObjectNesting() throws Exception {
	    String testJSON="{\"mainID\":\"ID\",\"nested\":{\"sub\":\"xxx\"}}";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><mainID>ID</mainID><nested><sub>xxx</sub></nested></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	@Test
	public void testSampleJSONNotWorkingInStandardLibraries() throws Exception {
	    String testJSON="[{\"id\":\"77\",\"published\":\"2017-01-05 14:37:23.778051+00\",\"resource\":{\"name\":\"persons\",\"id\":\"3b6a4a60-705b-4060-93ae-7b649258009d\",\"version\":\"application/vnd.hedtech.integration.v3+json\"},\"operation\":\"created\",\"contentType\":\"resource-representation\",\"content\":{\"addresses\":[],\"dateOfBirth\":null,\"credentials\":[{\"credentialId\":\"E948258C2894E75D5B37C75EECAAD3AC\",\"credentialType\":\"Banner UDC ID\",\"effectiveEndDate\":null,\"effectiveStartDate\":null},{\"credentialId\":\"A00010343\",\"credentialType\":\"Banner ID\",\"effectiveEndDate\":null,\"effectiveStartDate\":null}],\"dateDeceased\":null,\"emails\":[],\"ethnicity\":{\"guid\":\"17600ff6-31da-40db-8fae-7ae370772c4f\"},\"guid\":\"3b6a4a60-705b-4060-93ae-7b649258009d\",\"metadata\":{\"dataOrigin\":\"Banner\"},\"names\":[{\"firstName\":\"Simon\",\"lastName\":\"Mignolet\",\"middleName\":null,\"nameType\":\"Primary\",\"pedigree\":null,\"preferredName\":null,\"lastNamePrefix\":null,\"title\":\"Mr\"}],\"phones\":[],\"races\":[],\"roles\":[],\"gender\":\"Unknown\"},\"publisher\":{\"id\":\"fdec2f05-24d3-442d-b35f-2571464d48c8\",\"applicationName\":\"Banner Student CRUD Support\"}},{\"id\":\"78\",\"published\":\"2017-01-05 14:37:24.296963+00\",\"resource\":{\"name\":\"persons\",\"id\":\"3b6a4a60-705b-4060-93ae-7b649258009d\",\"version\":\"application/vnd.hedtech.integration.v3+json\"},\"operation\":\"created\",\"contentType\":\"resource-representation\",\"content\":{\"addresses\":[],\"dateOfBirth\":null,\"credentials\":[{\"credentialId\":\"E948258C2894E75D5B37C75EECAAD3AC\",\"credentialType\":\"Banner UDC ID\",\"effectiveEndDate\":null,\"effectiveStartDate\":null},{\"credentialId\":\"A00010343\",\"credentialType\":\"Banner ID\",\"effectiveEndDate\":null,\"effectiveStartDate\":null}],\"dateDeceased\":null,\"emails\":[],\"ethnicity\":{\"guid\":\"17600ff6-31da-40db-8fae-7ae370772c4f\"},\"guid\":\"3b6a4a60-705b-4060-93ae-7b649258009d\",\"metadata\":{\"dataOrigin\":\"Banner\"},\"names\":[{\"firstName\":\"Simon\",\"lastName\":\"Mignolet\",\"middleName\":null,\"nameType\":\"Primary\",\"pedigree\":null,\"preferredName\":null,\"lastNamePrefix\":null,\"title\":\"Mr\"}],\"phones\":[],\"races\":[],\"roles\":[],\"gender\":\"Unknown\"},\"publisher\":{\"id\":\"fdec2f05-24d3-442d-b35f-2571464d48c8\",\"applicationName\":\"Banner Student CRUD Support\"}}]";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><id>77</id><published>2017-01-05 14:37:23.778051+00</published><resource><name>persons</name><id>3b6a4a60-705b-4060-93ae-7b649258009d</id><version>application/vnd.hedtech.integration.v3+json</version></resource><operation>created</operation><contentType>resource-representation</contentType><content><addresses xsi:nil=\"true\"/><dateOfBirth xsi:nil=\"true\"/><credentials><credentialId>E948258C2894E75D5B37C75EECAAD3AC</credentialId><credentialType>Banner UDC ID</credentialType><effectiveEndDate xsi:nil=\"true\"/><effectiveStartDate xsi:nil=\"true\"/></credentials><credentials><credentialId>A00010343</credentialId><credentialType>Banner ID</credentialType><effectiveEndDate xsi:nil=\"true\"/><effectiveStartDate xsi:nil=\"true\"/></credentials><dateDeceased xsi:nil=\"true\"/><emails xsi:nil=\"true\"/><ethnicity><guid>17600ff6-31da-40db-8fae-7ae370772c4f</guid></ethnicity><guid>3b6a4a60-705b-4060-93ae-7b649258009d</guid><metadata><dataOrigin>Banner</dataOrigin></metadata><names><firstName>Simon</firstName><lastName>Mignolet</lastName><middleName xsi:nil=\"true\"/><nameType>Primary</nameType><pedigree xsi:nil=\"true\"/><preferredName xsi:nil=\"true\"/><lastNamePrefix xsi:nil=\"true\"/><title>Mr</title></names><phones xsi:nil=\"true\"/><races xsi:nil=\"true\"/><roles xsi:nil=\"true\"/><gender>Unknown</gender></content><publisher><id>fdec2f05-24d3-442d-b35f-2571464d48c8</id><applicationName>Banner Student CRUD Support</applicationName></publisher></res><res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><id>78</id><published>2017-01-05 14:37:24.296963+00</published><resource><name>persons</name><id>3b6a4a60-705b-4060-93ae-7b649258009d</id><version>application/vnd.hedtech.integration.v3+json</version></resource><operation>created</operation><contentType>resource-representation</contentType><content><addresses xsi:nil=\"true\"/><dateOfBirth xsi:nil=\"true\"/><credentials><credentialId>E948258C2894E75D5B37C75EECAAD3AC</credentialId><credentialType>Banner UDC ID</credentialType><effectiveEndDate xsi:nil=\"true\"/><effectiveStartDate xsi:nil=\"true\"/></credentials><credentials><credentialId>A00010343</credentialId><credentialType>Banner ID</credentialType><effectiveEndDate xsi:nil=\"true\"/><effectiveStartDate xsi:nil=\"true\"/></credentials><dateDeceased xsi:nil=\"true\"/><emails xsi:nil=\"true\"/><ethnicity><guid>17600ff6-31da-40db-8fae-7ae370772c4f</guid></ethnicity><guid>3b6a4a60-705b-4060-93ae-7b649258009d</guid><metadata><dataOrigin>Banner</dataOrigin></metadata><names><firstName>Simon</firstName><lastName>Mignolet</lastName><middleName xsi:nil=\"true\"/><nameType>Primary</nameType><pedigree xsi:nil=\"true\"/><preferredName xsi:nil=\"true\"/><lastNamePrefix xsi:nil=\"true\"/><title>Mr</title></names><phones xsi:nil=\"true\"/><races xsi:nil=\"true\"/><roles xsi:nil=\"true\"/><gender>Unknown</gender></content><publisher><id>fdec2f05-24d3-442d-b35f-2571464d48c8</id><applicationName>Banner Student CRUD Support</applicationName></publisher></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	//This case results in ambigious output but this is an edge case and I decided not to add extra xml elements to support this case
	@Test
	public void testObjectWithTwoFieldsNestedInObject() throws Exception {
	    String testJSON="{{\"id\":\"77\",\"id\":\"78\"},{\"id\":\"79\",\"id\":\"80\"}}";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><id>77</id><id>78</id><id>79</id><id>80</id></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	//This case results in ambigious output but this is an edge case and I decided not to add extra xml elements to support this case
	@Test
	public void testObjectWithTwoFieldsNestedInArrayNestedInArray() throws Exception {
	    String testJSON="[[{\"id\":\"77\",\"id\":\"78\"},{\"id\":\"79\",\"id\":\"80\"}]]";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><id>77</id><id>78</id><id>79</id><id>80</id></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	@Test
	public void testRootObjectWithMutipleValues() throws Exception {
	    String testJSON="{\"id\":\"77\",\"id\":\"78\",\"id\":\"79\"}";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><id>77</id><id>78</id><id>79</id></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	@Test
	public void testRootArrayWithMutipleValues() throws Exception {
	    String testJSON="[\"77\",\"78\",\"79\"]";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">77</res><res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">78</res><res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">79</res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	@Test
	public void testInvlaidHTMLCharsInValues() throws Exception {
	    String testJSON="\"test\":\"<>\\\"'&\"";
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><test>&lt;&gt;&quot;&apos;&amp;</test></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	@Test
	public void testJSONEscapeCharsInValue() throws Exception {
	    String testJSON="\"test\":\"\\\"\\\\\b\n\r\t\""; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><test>&quot;\\\n\r\t</test></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	@Test
	public void testJSONQuoteCharsInValue() throws Exception {
	    String testJSON="\"test\":\"\\\"\""; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><test>&quot;</test></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	@Test
	public void testJSONUnicodeCharsInValue() throws Exception {
		//Good place to find examples http://jrgraphix.net/r/Unicode/20A0-20CF
	    String testJSON="\"test\":\"☺☕é¡₤€\""; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><test>☺☕é¡₤€</test></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}

	@Test
	public void testJEscapedUnicodeInValue() throws Exception {
		//Good place to find examples http://jrgraphix.net/r/Unicode/20A0-20CF
	    String testJSON="\"test\":\"\\u005C\""; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><test>\\</test></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	@Test
	public void testInvalidXMLInFieldName() throws Exception {
	    String testJSON="{\"&^%test\":\"Value1\",\"1test\":\"Value2\",\"abc test\":\"Value3\",\"te☺st\":\"Value4\",\"☺\":\"CompletlyInvalid\"}"; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><test>Value1</test><test>Value2</test><abctest>Value3</abctest><test>Value4</test><element>CompletlyInvalid</element></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	@Test
	public void testInvalidXMLCompletlyValidStartsWithNum() throws Exception {
	    String testJSON="{\"1☺\":\"CompletlyInvalidStartWithNum\"}"; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><element1>CompletlyInvalidStartWithNum</element1></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	@Test
	public void testInvalidXMLElementNameStartsWithXML() throws Exception {
	    String testJSON="{\"xml\":\"xml\",\"xMl\":\"xMl\",\"xMl123\":\"xMl123\",\"xMlabc\":\"xMlabc\",\"axMlabc\":\"axMlabc\"}"; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><_xml>xml</_xml><_xMl>xMl</_xMl><_xMl123>xMl123</_xMl123><_xMlabc>xMlabc</_xMlabc><axMlabc>axMlabc</axMlabc></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	@Test
	public void testEmptyValueWithQuotesReturnsEmptyString() throws Exception {
	    String testJSON="{\"value\":\"\", \"sec\":\"a\"}"; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><value></value><sec>a</sec></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
	}
	
	@Test
	public void testQuotesInValue() throws Exception {
	    String testJSON="{\"value\":\"\\\"\", \"sec\":\\\"}"; //
	    String expectedResult = "<res xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><value>&quot;</value><sec>&quot;</sec></res>";
		
		runExpcetedResultTest(testJSON, expectedResult);
		
	}
}
