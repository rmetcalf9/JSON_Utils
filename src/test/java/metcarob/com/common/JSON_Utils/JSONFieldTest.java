package metcarob.com.common.JSON_Utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class JSONFieldTest {

	@Test
	public void testRemoveQuotes() {
		String inp = "\"d123d\"";
		String out = JSONField.removeQuotes(inp);
		if (!out.equals("d123d")) fail("Failed");
	}

}
