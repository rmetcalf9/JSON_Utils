package metcarob.com.common.JSON_Utils;

import static org.junit.Assert.*;

import metcarob.com.common.JSON_Utils.JSONTokenizer;

import java.util.List;

import org.junit.Test;

public class JSONTokenizerTest {
	
	private void outputResult(List<String> p_arr) {
		for (int c=0;c<p_arr.size();c++) {
			System.out.println(c + ": " + p_arr.get(c));
		}
	}

	@Test
	public void testBasicTokenizer() throws Exception {
		String JSON = "[{\"aa\"   : \"b \\\"b\",cc:\"\'r\"},   {\"aa\":\"b\\b\",cc:\"f  \"}]  ";
		
		String[] expectedResult = {
				"[",
				"{",
				"\"aa\"",
				":",
				"\"b \\\"b\"",
				",",
				"cc",
				":",
				"\"\'r\"",
				"}",
				",",
				"{",
				"\"aa\"",
				":",
				"\"b\\b\"",
				",",
				"cc",
				":",
				"\"f  \"",
				"}",
				"]"				
		};
		
		List<String> res = (new JSONTokenizer(JSON)).getTokens();
		if (res.size()!=expectedResult.length) {
			outputResult(res);
			fail("Size Mismatch");
		}
		for (int c=0;c<res.size();c++) {
			if (!res.get(c).equals(expectedResult[c])) {
				outputResult(res);
				fail("Token " + c + " mismatched (" + expectedResult[c] + "/" + res.get(c) + ")");
			}
		}
	}

}
