package metcarob.com.common.JSON_Utils;

import java.util.List;

public class JSON_Converter {
	
	public JSON_Converter() throws Exception {
		throw new Exception("Trying to create instance of static class");
	}
	
	public static JSONBase getJSONObject(String p_JSON) throws Exception {
		List<String> tokenized = (new JSONTokenizer(p_JSON)).getTokens();
		
		JSONBase obj = null;
		if (tokenized.size()==0) return null;
		if (tokenized.get(0).equals("[")) {
			obj = parseJSONValueTokens(tokenized, null);
		} else {
			obj = parseJSONTokens(tokenized, null);
		}
		return obj;
	}
	
	public static String ConvertJSONtoXML(String p_JSON) throws Exception {
		JSONBase obj = getJSONObject(p_JSON);
		if (null==obj) return "";	
				
		return obj.getXML(null);
	}
	
	public static JSONBase parseJSONValueTokens(List<String> tokens, JSONBase p_parent) throws Exception{
		if (tokens.size()==0) return null;
		//System.out.println("parseJSONValueTokens:" + tokens.get(0));
		if (tokens.get(0).equals("{")) return new JSONObject(tokens,p_parent);
		if (tokens.get(0).equals("[")) return new JSONArray(tokens,p_parent);
		return new JSONValue(tokens,p_parent);		
	}
	
	public static JSONBase parseJSONTokens(List<String> tokens, JSONBase p_parent) throws Exception{
		if (tokens.size()==0) return null;
		//System.out.println("parseJSONTokens:" + tokens.get(0));
		if (tokens.get(0).equals("{")) return new JSONObject(tokens,p_parent);
		if (tokens.get(0).equals("[")) return new JSONArray(tokens,p_parent);
		return new JSONField(tokens,p_parent);
	}
	
}
