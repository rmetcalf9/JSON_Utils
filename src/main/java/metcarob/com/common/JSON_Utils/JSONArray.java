package metcarob.com.common.JSON_Utils;

import java.util.ArrayList;
import java.util.List;

/*
 * This is a JSON array
 * JSON Arrays can only have values or objects inside them
 */
public class JSONArray extends JSONBase {
	private List<JSONBase> m_children = new ArrayList<JSONBase>();

	public JSONArray(List<String> p_tokenized, JSONBase p_parent) throws Exception {
		super(p_parent);
		if (!p_tokenized.get(0).equals("[")) throw new Exception("Not an array");
		p_tokenized.remove(0);
		
		while (!p_tokenized.get(0).equals("]")) {
			if (p_tokenized.get(0).equals(",")) {
				p_tokenized.remove(0);
			} else {
				m_children.add(JSON_Converter.parseJSONValueTokens(p_tokenized, this));
			};
		}
		
		//Remove the end token
		p_tokenized.remove(0);
	}

	@Override
	public String getXML(JSONField p_par) throws Exception {
		String fieldName = "";
		String xml = "";
		
		boolean outputtag = true;
		if (m_parent!=null) outputtag = false; 
		
		if (m_children.size()==0) {
			if (null!=p_par) {
				fieldName = p_par.getFieldName();
				xml += XMLUtils.EleSelfClose(fieldName, "xsi:nil=\"true\"");
				return xml;
			} else {
				//Empty Array as the root
				// in this case we will not output anything at all
				// this allows anything looking at the returned xml to iterate over the item
				// and get 0 iterations without special logic
				return "";
			}
		}
		if (null==p_par) {
			for (int c=0;c<m_children.size();c++) {
				if (outputtag) {
					xml += XMLUtils.rootElement(m_children.get(c).getXML(p_par));
				} else {
					xml += m_children.get(c).getXML(p_par);
				};
			}
		} else {
			for (int c=0;c<m_children.size();c++) {
				xml += m_children.get(c).getXML(p_par);
			}
		}
		return xml;
	}

	@Override
	public String getXML() throws Exception {
		String fieldName = "";
		String xml = "";
		if (m_children.size()==0) {
			return "";
		}
		for (int c=0;c<m_children.size();c++) {
			xml += m_children.get(c).getXML(null);
		}
		return xml;
	}

}
