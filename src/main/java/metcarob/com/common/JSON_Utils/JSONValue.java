package metcarob.com.common.JSON_Utils;

import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

public class JSONValue extends JSONBase {
	String m_value = "";

	
	public JSONValue(List<String> p_tokenized, JSONBase p_parent) throws Exception {
		super(p_parent);
		m_value = replaceJSONEscapeCharacters(JSONField.removeQuotes(p_tokenized.get(0)));
		if (JSONConstants.isStringInCharArray(m_value, JSONConstants.JSON_Tokens)) throw new InvalidFieldValueException("Invalid field value " + m_value);
		p_tokenized.remove(0);
	}

	@Override
	public boolean isNull() {return m_value.equals("null");}

	@Override
	public String getXML(JSONField p_par) throws Exception {
		String fieldName = null;
		if (null!=p_par) {
			fieldName = p_par.getFieldName();
		}
		String xml = "";
		if (isNull()) {
			xml = XMLUtils.EleSelfClose(fieldName, "xsi:nil=\"true\"");
			return xml;
		}
		
		String content = "";
		if (null==fieldName) {
			content += getXML();
		} else {
			content += XMLUtils.Ele(fieldName, getXML());
		}
		
		if (null==m_parent) {
			xml += XMLUtils.rootElement(content);
		} else {
			xml += content;
		}
		
		return xml;
	}
	
	private String replaceJSONEscapeCharacters(String p_JSONValue) throws Exception {
		return StringEscapeUtils.unescapeJson(p_JSONValue);

	}
	
	private String replaceInvalidXMLCharacters(String p_value) {
		//Not supporting the backspace character
		String initial = p_value
			.replaceAll("\b", "")
		;
		return StringEscapeUtils.escapeXml11(initial);
	}

	@Override
	public String getXML() throws Exception {
		if (isNull()) return "";
		return replaceInvalidXMLCharacters(getStringValue());
	}

	@Override
	public String getStringValue() throws Exception {
		return JSONField.removeQuotes(m_value);
	};

	
	
}
