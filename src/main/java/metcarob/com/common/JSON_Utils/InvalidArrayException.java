package metcarob.com.common.JSON_Utils;

public class InvalidArrayException extends Exception {

	public InvalidArrayException(String p_msg) {
		super(p_msg);
	}
	
}
