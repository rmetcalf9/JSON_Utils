package metcarob.com.common.JSON_Utils;

public class InvalidFieldValueException extends Exception {

	public InvalidFieldValueException(String p_msg) {
		super(p_msg);
	}

}
