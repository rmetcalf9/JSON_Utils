package metcarob.com.common.JSON_Utils;

import java.util.ArrayList;
import java.util.List;

/*
 * Class which will produce a tokenized JSON string array
 * has the JSON token rules build in
 * 
 * a tokenized string is an array of strings with a single token in each string
 * e.g.
 * 
 * [{"aa":"bb",cc:"r"},   {"aa":"bb",cc:"f"}]
 * should result in
 * [
 * {
 * "aa"
 * :
 * "b \"b"
 * ,
 * cc
 * :
 * "r"
 * }
 * ,
 * {
 * "aa"
 * :
 * "b\\b"
 * ,
 * cc
 * :
 * "f "
 * }
 * ]
 */
public class JSONTokenizer {
	List<String> m_tokens = new ArrayList<String>();
	
	char[] m_JSON_Escape_Tokens = {
			'\\'
	};
	char[] m_Literal_Tokens = { //MUST ALL BE SINGLE CHARS
			'\"'
	};
	char[] m_WhiteSpaceTokens = { 
			' ', '\t', '\r', '\n'
	};
	
	public JSONTokenizer(String p_JSON) throws Exception {
		
		boolean lastCharWasEscape = false;
		char literal_end = '\0';
		String cur_token = "";
		char cur = '\0';
		for (int c=0;c<p_JSON.length();c++) {
			cur = p_JSON.charAt(c);
			
			if (lastCharWasEscape) {
				//ignore anything after an escape char
				cur_token += cur;				
			} else {
				if (literal_end=='\0') {
					if (!JSONConstants.isCharInCharArray(cur,m_WhiteSpaceTokens)) {
						cur_token += cur;
					}
				
					if (JSONConstants.isStringInCharArray(cur_token,JSONConstants.JSON_Tokens)) {
						m_tokens.add(cur_token);
						cur_token = "";
					} else if (JSONConstants.isStringInCharArray(cur_token,m_Literal_Tokens)) {
						literal_end = cur_token.charAt(0);
					} else if (JSONConstants.isCharInCharArray(cur,JSONConstants.JSON_Tokens)) {
						
						m_tokens.add(cur_token.substring(0, cur_token.length()-1));
						cur_token = "";	
						cur_token += cur;
						m_tokens.add(cur_token);
						cur_token = "";	
					}
				
				} else {
					cur_token += cur;
	
					if (cur==literal_end) {
						literal_end = '\0';
						m_tokens.add(cur_token);
						cur_token = "";					
					}
				}
			}
			
			if (JSONConstants.isCharInCharArray(cur,m_JSON_Escape_Tokens)) {
				lastCharWasEscape = true;
			} else {
				lastCharWasEscape = false;
			}
		}
		if (cur_token.length()>0) m_tokens.add(cur_token);
	}
	

	public List<String> getTokens() {
		return m_tokens;
	}
}
