package metcarob.com.common.JSON_Utils;

public class InvalidFieldException extends Exception {

	public InvalidFieldException(String p_msg) {
		super(p_msg);
	}

}
