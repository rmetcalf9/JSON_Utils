package metcarob.com.common.JSON_Utils;

/*
 * Utility class to hold common JSONConstants
 */
public class JSONConstants {

	public static char[] JSON_Tokens = {
			'[',']','{','}',',',':'
	};
	
	public static boolean isStringInCharArray(String cur_token, char[] tokenSet) {
		if (cur_token.length()!=1) return false;
		char toComp = cur_token.charAt(0);
		for (int c=0;c<tokenSet.length;c++) {
			if (tokenSet[c]==toComp) return true;			
		}
		return false;
	}
	public static boolean isCharInCharArray(char cur_token, char[] tokenSet) {
		for (int c=0;c<tokenSet.length;c++) {
			if (tokenSet[c]==cur_token) return true;			
		}
		return false;
	}
	

}
