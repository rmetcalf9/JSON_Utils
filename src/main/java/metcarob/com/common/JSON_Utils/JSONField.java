package metcarob.com.common.JSON_Utils;

import java.util.List;


/*
 * A field is a value pair "e" : "b"
 */
public class JSONField extends JSONBase {
	private String m_field_name = "";
	private JSONBase m_field_value = null;
	
	public JSONField(List<String> p_tokenized, JSONBase p_parent) throws Exception {
		super(p_parent);
		if (!p_tokenized.get(1).equals(":")) throw new InvalidFieldException("Invalid Field - " + p_tokenized.get(0) + " (missing :)");
		
		m_field_name = p_tokenized.get(0);
		
		p_tokenized.remove(0);
		p_tokenized.remove(0);

		//Could be object or value
		m_field_value = JSON_Converter.parseJSONValueTokens(p_tokenized,this);

		
	}
	
	public static String removeQuotes(String p_inp) {
		//System.out.println("Remove quotes :" + p_inp + ":");
		
		if (p_inp.length()<2) return p_inp;
		if (p_inp.charAt(0)=='\"') {
			if (p_inp.charAt(p_inp.length()-1)=='\"') {
				return p_inp.substring(1, p_inp.length()-1);
			}
		}
		return p_inp;
	}

	@Override
	public String getXML(JSONField p_par) throws Exception {
		if (null==m_parent) {
			return XMLUtils.rootElement(m_field_value.getXML(this));
		}
		return m_field_value.getXML(this);
	}

	public String getFieldName() throws Exception {
		return removeQuotes(m_field_name);
	}

	@Override
	public String getXML() throws Exception {
		return m_field_value.getXML(this);
	}

	public JSONBase getValue() {
		return m_field_value;
	}

}
