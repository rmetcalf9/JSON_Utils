package metcarob.com.common.JSON_Utils;

public abstract class JSONBase {
	protected JSONBase m_parent = null;
	public JSONBase(JSONBase p_parent) throws Exception {
		m_parent = p_parent;
	}
	
	//get XML with root tag
	public abstract String getXML(JSONField p_par) throws Exception;

	//get XML without root tag
	public abstract String getXML() throws Exception;

	public boolean isNull() throws Exception {return false;}

	public String getStringValue() throws Exception {
		throw new Exception("Type has no string value");
	}
	
}
