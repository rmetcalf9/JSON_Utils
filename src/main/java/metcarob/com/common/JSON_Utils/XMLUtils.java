package metcarob.com.common.JSON_Utils;

/*
 * Some basic quick xml helper functions to co-locate xml validation of strings
 */
public class XMLUtils {
	
	private static boolean validXMLElementStartChar(char c) {
		if (c=='_') return true;
		if (Character.isLetter(c)) return true;
		return false;
	}
	private static boolean validXMLElementChar(char c) {
		if (c=='-') return true;
		if (c=='_') return true;
		if (c=='.') return true;
		if (Character.isLetter(c)) return true;
		if (Character.isDigit(c)) return true;
		return false;
	}
	
	private static String validXMLElementCharsOnly(String p_inp) {
		String ret = "";
		for (int c=0;c<p_inp.length();c++) {
			if (validXMLElementChar(p_inp.charAt(c))) ret += p_inp.charAt(c);
		}
		return ret;
	}
	
	private static String getValidXMLFieldNameFromJSON(String p_JSONFieldName) throws Exception {
		//http://www.w3schools.com/xml/xml_elements.asp
		//  Section XML Naming Rules
		//    Element names are case-sensitive
		//    Element names must start with a letter or underscore
		//    Element names cannot start with the letters xml (or XML, or Xml, etc)
		//    Element names can contain letters, digits, hyphens, underscores, and periods
		//    Element names cannot contain spaces
		if (p_JSONFieldName==null) return "element";
		if (p_JSONFieldName.length()==0) return "element";
		
		String startCharsRemoved="";
		while (!validXMLElementStartChar(p_JSONFieldName.charAt(0))) {
			if (p_JSONFieldName.length()==1) break;
			startCharsRemoved += p_JSONFieldName.charAt(0);
			p_JSONFieldName = p_JSONFieldName.substring(1);			
		}		
		
		String ret = validXMLElementCharsOnly(p_JSONFieldName);
		
		if (ret.length()==0) {
			//System.out.println("SCR:" + startCharsRemoved);
			//System.out.println("SCR2:" + validXMLElementCharsOnly(startCharsRemoved));
			//There were no valid chars (apart from any possible start chars)
			ret = "element" + validXMLElementCharsOnly(startCharsRemoved);
		}
		
		//Check if it starts with xml
		if (ret.length()>2) {
			if (ret.substring(0,3).equalsIgnoreCase("xml")) ret = "_" + ret;
		}
		
		return ret;
	}

	public static String rootElement(String p_Content) throws Exception {
		return Ele("res",p_Content,"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
	}
	
	public static String Ele(String p_Name, String p_Content) throws Exception {
		return Ele(p_Name,p_Content,"");
	}
	public static String Ele(String p_Name, String p_Content, String p_Attributes) throws Exception {
		String field_name = getValidXMLFieldNameFromJSON(p_Name);
		String ret = "";
		ret += "<" + field_name;
		if (p_Attributes.length()>0) ret += " " + p_Attributes;
		ret += ">";
		ret += p_Content;
		ret += "</" + field_name + ">";
		return ret;
	}

	public static String EleSelfClose(String p_Name, String p_Attributes) throws Exception {
		String field_name = getValidXMLFieldNameFromJSON(p_Name);
		String ret = "";
		ret += "<" + field_name;
		if (p_Attributes.length()>0) ret += " " + p_Attributes;
		ret += "/>";
		return ret;
	}
}
