package metcarob.com.common.JSON_Utils;

import java.util.ArrayList;
import java.util.List;

/*
 * Represents a JSONObject with mutiple children
 *  
 */
public class JSONObject extends JSONBase {
	private List<JSONBase> m_children = new ArrayList<JSONBase>();

	public JSONObject(List<String> p_tokenized, JSONBase p_parent) throws Exception {
		super(p_parent);
		if (!p_tokenized.get(0).equals("{")) throw new Exception("Not an object");
		p_tokenized.remove(0);
		
		while (!p_tokenized.get(0).equals("}")) {
			if (p_tokenized.get(0).equals(",")) {
				p_tokenized.remove(0);
			} else {
				m_children.add(JSON_Converter.parseJSONTokens(p_tokenized, this));
			};
		}
		//remove closing token
		p_tokenized.remove(0);
	}

	@Override
	public String getXML(JSONField p_par) throws Exception {
		String xml = "";

		/*
		if (p_par==null) {
			if (m_parent==null) {
				System.out.println("JSONObject p_par - p_par = null - m_parent is null");
			} else {
				System.out.println("JSONObject p_par - p_par = null - m_parent is not null");				
			}
		} else {
			System.out.println("JSONObject p_par - p_par not null");
		}*/
		
		boolean outputrestag = true;
		if (p_par==null && m_parent!=null) {
			//System.out.println("Logical suppress tag");
			outputrestag = false;
		}
		
		String content = "";
		for (int c=0;c<m_children.size();c++) {
			content += m_children.get(c).getXML(p_par);
		}
		
		if (outputrestag) {
			xml += XMLUtils.rootElement(content);
		} else {
			xml += content;
		}
		if (null==p_par) return xml;
		
		String fieldName = p_par.getFieldName();
				
		return XMLUtils.Ele(fieldName, getXML());
	}

	@Override
	public String getXML() throws Exception {
		String xml = "";
		
		for (int c=0;c<m_children.size();c++) {
			xml += m_children.get(c).getXML();
		}
		return xml;
	}

	public JSONField getChildField(String p_fieldName) throws Exception {
		for (int c=0;c<m_children.size();c++) {
			//System.out.println(m_children.get(c).getClass().getName());
			if (m_children.get(c) instanceof JSONField) {
				JSONField f = (JSONField) m_children.get(c);
				//System.out.println(f.getFieldName());
				if (f.getFieldName().equals(p_fieldName)) {
					return f;
				}
			}
		}
		return null;
	}

}
